<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Carbon\Carbon;


class UserController extends Controller
{
    //

    public $successStatus = 200;


    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {


        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $check = User::where('email',request('email'))->first();
            if($check->status=='inactive'){
                return response()->json(['error' => 'Account Suspended'], 401);
            }

            $user = Auth::user();
            $role = $user->roles->first();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['info'] = $user;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['status'] = 'active';
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        $user->attachRole(2);

        return response()->json(['success' => $success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function createUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'role' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $cUser = createUsers($request->name, $request->email, $request->password, $request->role, $request->phone_number);


        if($cUser==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "User Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }



    public function createRole(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = createRoles($request->name, $request->display_name, $request->description );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Role Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function createPermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = createPermissions($request->name, $request->display_name, $request->description );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Permission Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function getRoles()
    {
        $cRole = getRoles();

        return response()->json(['success' => $cRole], $this->successStatus);
    }

    public function getUsers()
    {
        $cRole = getUsersMod();

        return response()->json(['success' => $cRole], $this->successStatus);
    }

    public function getUser($id)
    {
        $user = getUser($id);

        return response()->json(['success' => $user], $this->successStatus);
    }




    public function getRolePermission($role_id)
    {
        $cRole = getRolePermissions($role_id);

        return response()->json(['success' => $cRole], $this->successStatus);
    }

    public function getUserRole($user_id)
    {
        $cRole = getUserRolePermissions($user_id);

        $data = $cRole->toArray();
        $role_id =    $data[0]['role']['role_id'];
        $permissions = getRolePermissions($role_id);

        return response()->json(['success' => $cRole,'permissions' => $permissions], $this->successStatus);
    }

    public function attachPermissionsRole(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'permissions' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = attachPermissionsRoles($request->role, $request->permissions );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Permission Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function attachRolesUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'role' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = attachRolesUsers($request->user, $request->role );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Permission Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function removeRolesUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'role' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }



        $cRole = removeRolesUsers($request->user, $request->role );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Permission Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function removePermissionsRole(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'permissions' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = removePermissionsRoles($request->role, $request->permissions );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Permission Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function updateStatus($user_id, $status)
    {

        $cRole = updateUserStatus($user_id, $status);

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Error Occurred in Request";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function sendResetLink(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = cofirmEmail($request->email);

        if($cRole==200){
            $messageStatus = "Successful";
        }elseif($cRole==401){
            $messageStatus = "Email Dosnt Exit in our System";
        }else{
            $messageStatus = "Network Error Please Try Again";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function resetEmail($email, $code)
    {

        $cRole = cofirmEmilVeri($email, $code);

        if($cRole==200){
            $messageStatus = "Successful";
        }elseif($cRole==401){
            $messageStatus = "Email Dosnt Exit in our System";
        }else{
            $messageStatus = "Network Error Please Try Again";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function resetPassword(Request $request)
    {



        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = Auth::user();
        $cRole = resetPassword($user->id, $request->password);

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Network Error Please Try Again";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function getRole($role_id)
    {
        $cRole = getRole($role_id);

        return response()->json(['success' => $cRole], $this->successStatus);
    }

    public function getPermissions()
    {
        $cPermissions = getPermissions();

        return response()->json(['success' => $cPermissions], $this->successStatus);
    }

    public function updateRole($role_id,Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $cRole = updateRole($role_id, $request->name, $request->display_name, $request->description );

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "Role Already Exist";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }

    public function updateUserDetails($userID,$name,$phone,$role)
    {


        $cRole = updateUserDetails($userID,$name,$phone,$role);

        if($cRole==200){
            $messageStatus = "Successful";
        }else{
            $messageStatus = "User details updated";
        }

        return response()->json(['success' => $messageStatus], $this->successStatus);
    }
}
