<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;
use Validator;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $applicants = Candidate::get();
        return view('users.index',['candidates'=> $applicants]);
    }

    public function apply()
    {
        return view('users.details');
    }

    public function q2()
    {

        $array = array(1, 2, 3, 4, 5);
        $ans = $array[0];
        $use = $ans+1;



        for($i=0;$i<= count($array);$i++){
            $cur = $array[$i];
            if($cur != $use){
                return $use;
            }else{
                $ans = $array[$i];
                $use = $ans+1;
            }

        }


        return $use;
    }


    public function apply_form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'cover' => 'required',
            'resume' => 'required',
            'passport' => 'required',


        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()], 401);
        }

        $destinationPath = "resume";
        $getCandidates = Candidate::count();

        if($getCandidates >= 2){

            Session::flash('err', 'Sorry, We are only expecting 4 Applicants');

            return redirect()->route('apply');
        }

        $file = $request->file('passport');

        $fileName = str_random(5);
        $file->move($destinationPath, $fileName . $file->getClientOriginalName());

        $picture = $fileName . $file->getClientOriginalName();
        $picture_path = $destinationPath . "/" . $picture;


        $file2 = $request->file('resume');

        $fileName2 = str_random(5);
        $file2->move($destinationPath, $fileName2 . $file2->getClientOriginalName());

        $picture2 = $fileName2 . $file2->getClientOriginalName();
        $picture_path2 = $destinationPath . "/" . $picture2;

        $data = new Candidate();
        $data->f_name = $request->first;
        $data->s_name = $request->surname;
        $data->email = $request->email;
        $data->cover_letter = $request->cover;
        $data->phone_number = $request->phone;
        $data->passport = $picture_path;
        $data->resume = $picture_path2;
        $data->save();




        Session::flash('msg', 'Successful, Application Received');

        return back();
    }
}
