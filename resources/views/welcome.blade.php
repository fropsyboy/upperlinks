<!DOCTYPE html>
<html>
<head>

    <title> Adio Consultancy Group</title>

    <!-- For-Mobile-Apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="Adio Consultancy Group">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps -->

    <!-- Style -->
    <link rel="stylesheet" href="{{url('landing/css/font-awesome.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{url('landing/css/style.css')}}"            type="text/css" media="all">
    <!-- //Style -->

    <!-- Fonts -->
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Montserrat:400,700'             type="text/css" media="all">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' type="text/css" media="all">
    <!-- Fonts -->

    <!-- Font-Awesome-Icons-File -->
    <link rel="stylesheet" href="{{url('landing/fonts/fontawesome-webfont.ttf')}}" type="text/css" media="all">
    <!-- //Font-Awesome-Icons-File -->

</head>
<!-- //Head -->



<!-- Body -->
<body>
<div class="banner-info">
    <h1>Adio Consultancy Group</h1>
    <p class="agile-cc">We are hiring to fill the position of a Software Engineer !</p>
    <div class="wrapper agileinfo">

        <div class="newsletter agile">
            <p>Please Click Below to apply </p>

            <a href="{{route('apply')}}">
            <button class="submit ">Apply</button>
            </a>
        </div>

    </div>

    <div class="footer">
        <p> &copy; 2019 Adio Consultancy Group. All Rights Reserved | Design by <a href="">Upper Links LTD</a></p>
        <!--social-icons-->

        <!--/social-icons-->

    </div>
</div>
<!-- Custom-JavaScript-File-Links -->
<script type="text/javascript" src="{{url('landing/js/jquery-1.12.3.min.js')}}"></script>
<script type="text/javascript" src="{{url('landing/js/moment.js')}}"></script>
<script type="text/javascript" src=" {{url('landing/js/moment-timezone-with-data.js')}}"></script>
<script type="text/javascript" src="{{url('landing/js/timer.js')}}"></script>
<!-- //Custom-JavaScript-File-Links -->

</body>
<!-- //Body -->

</html>
