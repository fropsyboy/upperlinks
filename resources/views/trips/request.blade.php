@extends('layouts.master')
@section('tripActive')
    class = 'active'
@endsection
@section('content')

    <div class="container-fluid" >

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}

                    <div class="body table-responsive">

                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th style="width:30px;text-align: center;">
                                    S/N
                                </th>
                                <th style="text-align: center;">
                                    Pick-up
                                </th>

                                <th style="width:200px;text-align: center;">
                                    Created Time
                                </th>
                                <th>
                                    Destination
                                </th>

                                <th style="width:50px;text-align: center;">
                                    Status
                                </th>
                                <th style="width:50px;text-align: center;">
                                    Action
                                </th>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>

                            @foreach ($trips as $trip)

                                <tr>
                                    <td style="width:10px;text-align: center;">
                                        {{$i}}
                                    </td>
                                    <td style="width:200px;text-align: center;">

                                        {{$trip->address}}
                                    </td>
                                    <td style="width:200px;text-align: center; font-size: 12px;">

                                        {{$trip->created_at}}
                                    </td>
                                    <td style="width:100px;text-align: center;">

                                        {{$trip->destination}}

                                    </td>
                                    <td style="width:100px;text-align: center;">
                                        {{$trip->status}}


                                    </td>
                                    <td style="width:200px;text-align: center;">
                                        <a href="{{route('tripStatus',['id' => $trip->id,'status' => 'canceled'])}}"> <button class="btn btn-danger btn-xs">Cancel</button></a>
                                        <a href="{{route('tripStatus',['id' => $trip->id,'status' => 'accepted'])}}">  <button class="btn btn-success btn-xs">Accept</button></a>
                                            <a href="{{route('tripStatus',['id' => $trip->id,'status' => 'start'])}}">  <button class="btn btn-info btn-xs">Start</button></a>
                                                <a href="{{route('tripStatus',['id' => $trip->id,'status' => 'end'])}}">  <button class="btn btn-default btn-xs">End</button></a>
                                    </td>


                                </tr>


                            <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection