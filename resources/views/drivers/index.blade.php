@extends('layouts.master')
@section('driverActive')
    class = 'active'
@endsection
@section('content')

    <div class="container-fluid" >

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    <button class="btn btn-info btn-xs waves-effect m-r-20" data-toggle="modal" data-target="#addDriver" style="margin-left: 20px; margin-top: 20px;">Add Driver</button>

                    <div class="body table-responsive">

                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th style="width:30px;text-align: center;">
                                    S/N
                                </th>
                                <th style="width:200px;text-align: center;">
                                    Name
                                </th>
                                <th style="width:50px;text-align: center;">
                                    Email
                                </th>
                                <th style="width:50px;text-align: center;">
                                    Telephone
                                </th>
                                <th style="width:50px;text-align: center;">
                                    Licence No
                                </th>
                                <th style="width:50px;text-align: center;">
                                    Status(Trips)
                                </th>

                                <th style="width:50px;text-align: center;">
                                    Action
                                </th>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($drivers as $driver)

                                <tr>
                                    <td style="width:10px;text-align: center;">
                                        {{$i}}
                                    </td>
                                    <td style="width:200px;text-align: center;">
                                        <a href="{{route('driver_details',['id' => $driver->id,'user' => $driver->user_id])}}" style="color:deepskyblue">
                                            {{$driver->name}} {{$driver->middle_name}} {{$driver->last_name}}
                                        </a>
                                    </td>
                                    <td style="width:200px;text-align: center; font-size: 12px;">

                                        {{$driver->user_d->email}}

                                    </td>
                                    <td style="width:100px;text-align: center;">

                                            {{$driver->phone_number}}


                                    </td>
                                    <td style="width:150px;text-align: center;">

                                            {{$driver->licence_number}}


                                    </td>


                                    <td style="width:50px;">
                                        <form action="" method="post">
                                            {{csrf_field()}}


                                            @if ($driver->status == 'Available')
                                                <a href="" class="label label-success pull-left"  >Active </a>
                                            @else

                                                <a href="" class="label label-danger pull-left"  >In-Active</a>
                                            @endif

                                            <span class="label label-info pull-right">On Trip</span>

                                        </form>
                                    </td>

                                    <td>
                                        {{--<button class="btn btn-info btn-xs" >{{$driver->status}}</button>--}}
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <span class="label label-count">Change Status</span>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="{{route('status',['id' => $driver->id,'status' => 'Available'])}}">Available</a></li>
                                                <li><a href="{{route('status',['id' => $driver->id,'status' => 'On Leave'])}}">On Leave</a></li>
                                                <li><a href="{{route('status',['id' => $driver->id,'status' => 'Retired'])}}">Retired</a></li>
                                                <li><a href="{{route('status',['id' => $driver->id,'status' => 'Resigned'])}}">Resigned</a></li>
                                            </ul>
                                        </li>

                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection