@extends('layouts.master')
@section('driverActive')
    class = 'active'
@endsection
@section('content')


    <div class="row clearfix">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="card">
                <div class="header">
                    <button class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Assign</button>
                    &nbsp; <b style="font-size: 18px;"> Driver Management </b>

                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home_with_icon_title" data-toggle="tab">
                                <i class="material-icons">home</i> Overview
                            </a>
                        </li>

                        <li role="presentation">
                            <a href="#profile_with_icon_title" data-toggle="tab">
                                <i class="material-icons">email</i> TRIPS
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#messages_with_icon_title" data-toggle="tab">
                                <i class="material-icons">face</i> ASSIGNMENTS
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#settings_with_icon_title" data-toggle="tab">
                                <i class="material-icons">settings</i> SETTINGS
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">


                            {{--@if($user->assign)--}}
                                {{--<img src="https://maps.googleapis.com/maps/api/staticmap?center={{$assign->current_latitude}},{{$assign->current_longitude}}&zoom=14&size=700x350--}}
                                {{--&markers=color:blue%7Clabel:S%7C{{$assign->current_latitude}},{{$assign->current_longitude}}&markers=size:tiny%7Ccolor:green%7CDelta+Junction,AK--}}
                                {{--&markers=size:mid%7Ccolor:0xFFFF00%7Clabel:C%7CTok,AK&key=AIzaSyAohWoKp_8ZdV4GLetRCBuwsNYwcbCV-d4">--}}
                                {{--@else--}}
                                {{--<img src="{{url('').'/driver_image/ex_map.jpg'}}" style="width: 650px;height: 350px">--}}
                                {{--@endif--}}

                            @if($user->assign)
                                <div id="floating-panel">
                                    <input id="latlng" type="hidden" value="6.551124, 3.3656586" readonly="" >
                                    {{--<input id="submit" type="button" value="Display" class="form-horizontal">--}}
                                </div>
                                <div id="map" style="width:650px;height:350px;background:whitesmoke"></div>
                            @else
                            <img src="{{url('').'/driver_image/ex_map.jpg'}}" style="width: 650px;height: 350px">
                            @endif

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">

                            <div class="body table-responsive">

                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th style="width:30px;text-align: center;">
                                            Start
                                        </th>
                                        <th style="text-align: center;">
                                            End
                                        </th>

                                        <th style="width:200px;text-align: center;">
                                            Driver
                                        </th>
                                        <th>
                                            Duration
                                        </th>
                                        <th>
                                            Status
                                        </th>

                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="messages_with_icon_title">
                            <div class="body table-responsive">

                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th style="width:50px;text-align: center;">
                                            Start
                                        </th>
                                        <th style="width:50px;text-align: center;">
                                            End
                                        </th>

                                        <th style="width:200px;text-align: center;">
                                            Vehicle
                                        </th>



                                    </thead>
                                    <tbody>
                                    @foreach($assignments as $item)

                                    <tr>

                                    <td style="width:100px;text-align: center;">
                                       {{$item->created_at->toFormattedDateString()}}
                                    </td>
                                    <td style="width:100px;text-align: center;">
                                        {{$item->updated_at->toFormattedDateString()}}
                                    </td>

                                    <td style="width:150px;text-align: center;">
                                        {{$item->Vehicle_name}}
                                    </td>

                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="settings_with_icon_title">

                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div >

                                        <div class="body">
                                            <form method="post" action="{{route('update_driver')}}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <div class="row clearfix">

                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="phone" value="@if($driver->phone_number) {{$driver->phone_number}} @endif">
                                                                <label class="form-label">Phone Number</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="licence" value="@if($driver->licence_number) {{$driver->licence_number}} @endif">
                                                                <label class="form-label">Licence Number</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <select class="form-control show-tick" name="status" >
                                                                    <option value="@if($driver->status) {{$driver->status}} @endif">Select Status</option>
                                                                    <option>Available</option>
                                                                    <option>Un-available</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <textarea class="form-control" name="address" > @if($driver->address) {{$driver->address}} @endif</textarea>
                                                                <label class="form-label">Address</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <input type="file"  class="form-control" name="picture"  id="quantity" >


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="id" value="{{$driver->id}}">



                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group form-float">

                                                            <button type="submit" class="btn btn-success btn-lg">Save </button>

                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="card">
                <div class="header">

                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    @if($driver->image)
                        <img src="{{url('').'/'.$driver->image}}" style="width: 270px;height: 150px">
                    @else
                    <img src="{{url('').'/driver_image/ex.jpg'}}" style="width: 270px;height: 150px">
                    @endif
                </div>

                <div style="text-align: center">
                    <h3>
                       {{$driver->name}}, {{$driver->middle_name}} {{$driver->last_name}}
                    </h3>
                    <h6>
                        @if($assign)
                            {{$assign->name}}
                            @endif
                    </h6>
                    {{--<h6>@if($driver->name) {{$driver->name}} @endif</h6>--}}
                    <b style="font-size: 11px; color: green">
                        Driver's Status:
                        @if($driver->status == 'Available')
                       <span class="label label-info">{{$driver->status}}</span>
                            @else
                            <span class="label label-danger">{{$driver->status}}</span>
                            @endif

                    </b>
                           <p> <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#status">  Change Status</button>

                    <p>Total Number of Trips: <span class="label label-primary">15</span>
                    <p>
                        <button class="btn btn-info btn-lg" data-toggle="modal" data-target="#details">
                            View Details
                        </button>
                    </p>





                    <br><br>
                </div>


            </div>
        </div>

    </div>

    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Assignment</h4>
                    <hr>
                </div>
                <form method="post" action="{{route('assign_driver')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-5">
                           <b>Driver's Name:</b>  {{$driver->name}} {{$driver->middle_name}}
                            <input type="hidden" name="driver_id" value="{{$user->id}}">
                            <input type="hidden" name="driver_name" value="{{$driver->name}}  {{$driver->middle_name}}  ">
                        </div>
                        <div class="col-sm-1">

                        </div>

                        <div class="col-sm-6">
                            <select class="form-control show-tick" name="vehicle_id" data-live-search="true">
                                <option value="">-- Please select --</option>
                                @foreach($vehicles as $vehicle)


                                    <option value="{{$vehicle->id}}">{{$vehicle->name}} @if($vehicle->vehicle_c)  => <spam class="label label-info">{{$vehicle->vehicle_c->status}}</spam>  @endif </option>

                                    @endforeach

                            </select>
                        </div>


                    </div>
                    <br>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">Assign</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="details" tabindex="-1" role="dialog">

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Driver Details</h4>
                        <Hr>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row clearfix">

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->name) {{$driver->name}} @endif" readonly>
                                        <label class="form-label">First Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->middle_name) {{$driver->middle_name}} @endif" readonly>
                                        <label class="form-label">Middle Name</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->last_name) {{$driver->last_name}} @endif" readonly>
                                        <label class="form-label">Other Name</label>
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="row clearfix">

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->phone_number) {{$driver->phone_number}} @endif" readonly>
                                        <label class="form-label">Phone Number</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->gender) {{$driver->gender}} @endif" readonly>
                                        <label class="form-label">Gender</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->blood_group) {{$driver->blood_group}} @endif" readonly>
                                        <label class="form-label">Blood Group</label>
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="row clearfix">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($user->email) {{$user->email}} @endif" readonly>
                                        <label class="form-label">Email</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@if($driver->genotype) {{$driver->genotype}} @endif" readonly>
                                        <label class="form-label">Genotype</label>
                                    </div>
                                </div>
                            </div>





                        </div>
                        <div class="row clearfix">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea  class="form-control" readonly>{{$driver->address}} </textarea>
                                        <label class="form-label">Address</label>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <br>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="modal fade" id="status" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Update Status <b class="label label-default"> {{$driver->status}} </b> </h4>
                    <hr>
                </div>
                <br>
                <form method="post" action="{{route('update_status')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <b>Driver Name: </b>  {{$driver->name}} {{$driver->middle_name}}
                                <input type="hidden" value="{{$driver->id}}" name="id">
                            </div>
                            <div class="col-sm-1">

                            </div>

                            <div class="col-sm-6">
                                <select class="form-control show-tick" name="status">
                                    <option value="">-- Select Status --</option>
                                    <option value="Available">Available</option>
                                    <option value="On Leave">On Leave</option>
                                    <option value="Resigned">Resigned</option>
                                    <option value="Retired">Retired</option>

                                </select>
                            </div>


                        </div>
                        <br><br>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Update</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--<script>--}}
        {{--function myMap() {--}}
            {{--var position = new google.maps.LatLng(6.551124, 3.3656586) ;--}}
            {{--var mapOptions = {--}}
                {{--center: position,--}}
                {{--zoom: 17,--}}
                {{--mapTypeId: google.maps.MapTypeId.terrain--}}
            {{--}--}}
            {{--var map = new google.maps.Map(document.getElementById("map"), mapOptions);--}}
            {{--var image = '{{url('').'/driver_image/car2.png'}}';--}}
            {{--var marker = new google.maps.Marker({--}}
                {{--position: position,--}}
                {{--icon: image,--}}
                {{--title:"Current Position!"--}}
            {{--});--}}
            {{--marker.setMap(map);--}}
        {{--}--}}
    {{--</script>--}}

    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: 6.551, lng: 3.3656}
            });
            var geocoder = new google.maps.Geocoder;
            var infowindow = new google.maps.InfoWindow;

                geocodeLatLng(geocoder, map, infowindow);

        }

        function geocodeLatLng(geocoder, map, infowindow) {
            var input = document.getElementById('latlng').value;
            var latlngStr = input.split(',', 2);
            var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
            geocoder.geocode({'location': latlng}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        map.setZoom(11);
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map
                        });
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAohWoKp_8ZdV4GLetRCBuwsNYwcbCV-d4&callback=initMap"></script>
@endsection