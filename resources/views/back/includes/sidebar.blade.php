<aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{url('assets/images/user.png')}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li @yield('dashActive')>
                        <a href="{{route('dashboard')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li @yield('driverActive')>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Drivers</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{route('allDrivers')}}" class="menu-toggle">
                                    <span>View drivers</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('addDriver')}}" class="menu-toggle">
                                    <span>Add driver</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li @yield('vehicleActive')>
                        <a href="{{route('vehicles')}}">
                            <i class="material-icons">text_fields</i>
                            <span>Vehicles</span>
                        </a>
                    </li>
                    <li @yield('tripActive')>
                        <a href="{{route('trips')}}">
                            <i class="material-icons">layers</i>
                            <span>Trips</span>
                        </a>
                    </li>
                    <li @yield('userActive')>
                        <a href="{{route('reports')}}">
                            <i class="material-icons">layers</i>
                            <span>Users</span>
                        </a>
                    </li>
                    <li @yield('reportActive')>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">layers</i>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li @yield('settingActive')>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{route('categories')}}" class="menu-toggle">
                                    <span>Categories</span>
                                </a>
                            <li>
                                <a href="{{route('roles')}}" class="menu-toggle">
                                    <span>Roles</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('administrators')}}" class="menu-toggle">
                                    <span>Administrators</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="https//www.concept-nova.com">Concept Nova</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>