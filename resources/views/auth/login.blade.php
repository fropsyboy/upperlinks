<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <link href="{{url('/logins/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- //Custom Theme files -->
    <!-- web font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
    <!-- //web font -->
    <!-- js -->
    <script src="{{url('/logins/js/jquery.min.js')}}"></script>
    <script src="{{url('/logins/js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
    <!-- //js -->
</head>
<body>
<!-- main -->
<div class="main">
    <div class="login-form">
        <div class="sap_tabs w3ls-tabs">
            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                <ul class="resp-tabs-list">
                    <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Login</span></li>
                    {{--<li class="resp-tab-item" ><a href="{{route('register')}}"><span>Sign up</span></a></li>--}}
                </ul>
                <div class="clear"> </div>
                <div class="resp-tabs-container">
                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">

                        <div class="login-agileits-top">
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <p>E-Mail Address</p>
                                    <input type="text" name="email" value="{{ old('email') }}"  required autofocus>

                                    @if ($errors->has('email'))
                                        <span style="color:red">
                                        {{ $errors->first('email') }}
                                    </span>
                                    @endif
                                </div>


                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <p>Password</p>
                                    <input type="password" name="password" required=""/>
                                    @if ($errors->has('password'))
                                        <span style="color:red">
                                                {{ $errors->first('password') }}
                                    </span>
                                    @endif
                                </div>
                                <input type="checkbox" name="remember" value="" checked>
                                <label for="brand"><span></span> Remember me ?</label>
                                <input type="submit" value="LOGIN">


                            </form>
                        </div>
                        <div class="login-agileits-bottom">

                            <p><a href="{{ route('password.request')}}">Forgot password?</a> &nbsp; <a href="{{route('register')}}">Sign Up?</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- ResponsiveTabs js -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true   // 100% fit in a container
                });
            });
        </script>
        <!-- //ResponsiveTabs js -->
    </div>
</div>
<!-- //main -->
<!-- copyright -->
<footer class="footer">
    <div class="container-fluid">
        <div class="copyright">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, Personal Projects
        </div>
    </div>
</footer>
<!-- //copyright -->
</body>
</html>
