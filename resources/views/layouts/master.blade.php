<!DOCTYPE html>
<html>

@include('includes.head')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    @include('includes.header')
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        @include('includes.sidebar')
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        @include('includes.rightbar')
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        @yield('content')
    </section>

    @include('includes.script')
    <div class="modal fade" id="master_assign" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Assignment</h4>
                    <hr>
                </div>
                <form method="post" action="" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <select class="form-control show-tick" name="driver_id" data-live-search="true">
                                <option value="">-- Select Driver --</option>

                            </select>
                        </div>


                        <div class="col-sm-6">
                            <select class="form-control show-tick" name="vehicle_id" data-live-search="true">
                                <option value="">-- Select Vehicle --</option>

                            </select>
                        </div>


                    </div>
                    <br>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">Assign</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</body>



</html>
<!-- Jquery Core Js -->


