@extends('layouts.master')
@section('vehicleActive')
    class = 'active'
@endsection
@section('content')

    <div class="container-fluid" >

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}

                    <div class="body table-responsive">

                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th style="width:30px;text-align: center;">
                                    S/N
                                </th>
                                <th style="text-align: center;">
                                    Name
                                </th>
                                <th style="text-align: center;">
                                    Date Created
                                </th>
                                <th style="text-align: center;">
                                    Action
                                </th>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($vehicles as $vehicle)

                                <tr>
                                    <td style="width:30px;text-align: center;">
                                        {{$i}}
                                    </td>
                                    <td style="width:300px;text-align: center;">
                                        <a href="{{route('vehicles_details',['id' => $vehicle->id])}}" style="color:deepskyblue">
                                            {{$vehicle->name}}
                                        </a>
                                    </td>
                                    <td style="width:300px;text-align: center;">
                                        {{$vehicle->created_at->toDayDateTimeString()}}
                                    </td>

                                    <td style="width:80px;text-align: center;">
                                        {{--<button class="btn btn-info btn-xs" >{{$driver->status}}</button>--}}
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <span class="label label-count">Change Status</span>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="{{route('status_v',['id' => $vehicle->id,'status' => 'Available'])}}">Available</a></li>
                                                <li><a href="{{route('status_v',['id' => $vehicle->id,'status' => 'Sold'])}}">Sold</a></li>
                                                <li><a href="{{route('status_v',['id' => $vehicle->id,'status' => 'Damaged'])}}">Damaged</a></li>
                                            </ul>
                                        </li>

                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection