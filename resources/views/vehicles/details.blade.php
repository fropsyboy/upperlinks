@extends('layouts.master')
@section('vehicleActive')
    class = 'active'
@endsection
@section('content')


    <div class="row clearfix">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        <div class="card">
            <div class="header">
                <button class="btn btn-success waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Assign</button>
                  &nbsp; <b style="font-size: 18px;"> Vehicle Management </b>

                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home_with_icon_title" data-toggle="tab">
                            <i class="material-icons">home</i> Overview
                        </a>
                    </li>

                    <li role="presentation">
                        <a href="#profile_with_icon_title" data-toggle="tab">
                            <i class="material-icons">email</i> TRIPS
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#messages_with_icon_title" data-toggle="tab">
                            <i class="material-icons">face</i> ASSIGNMENTS
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#settings_with_icon_title" data-toggle="tab">
                            <i class="material-icons">settings</i> SETTINGS
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">


                            <img src="https://maps.googleapis.com/maps/api/staticmap?center=6.44443833333,3.43169833333&zoom=14&size=700x350
&markers=color:blue%7Clabel:S%7C6.44443833333,3.43169833333&markers=size:tiny%7Ccolor:green%7CDelta+Junction,AK
&markers=size:mid%7Ccolor:0xFFFF00%7Clabel:C%7CTok,AK&key=AIzaSyAohWoKp_8ZdV4GLetRCBuwsNYwcbCV-d4">

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">

                        <div class="body table-responsive">

                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th style="width:30px;text-align: center;">
                                       Start
                                    </th>
                                    <th style="text-align: center;">
                                        End
                                    </th>

                                    <th style="width:200px;text-align: center;">
                                        Driver
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Review
                                    </th>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages_with_icon_title">
                        <div class="body table-responsive">

                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th style="width:50px;text-align: center;">
                                        Start
                                    </th>
                                    <th style="width:50px;text-align: center;">
                                        End
                                    </th>

                                    <th style="width:200px;text-align: center;">
                                        Driver
                                    </th>



                                </thead>
                                <tbody>
                                @foreach($assignments as $item)

                                    <tr>

                                        <td style="width:100px;text-align: center;">
                                            {{$item->created_at->toFormattedDateString()}}
                                        </td>
                                        <td style="width:100px;text-align: center;">
                                            {{$item->updated_at->toFormattedDateString()}}
                                        </td>

                                        <td style="width:150px;text-align: center;">
                                            {{$item->driver_name}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="settings_with_icon_title">

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div >

                                    <div class="body">
                                        <form method="post" action="{{route('update_vehicle')}}" enctype="multipart/form-data">
                                            {{csrf_field()}}

                                            <input type="hidden" name="id" value="{{$vehicles->id}}">
                                            <input type="hidden" name="detail_id" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->id}} @endif">
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="name" value="@if($vehicles->name) {{$vehicles->name}} @endif">
                                                            <label class="form-label">Name</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="color" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->color}} @endif">
                                                            <label class="form-label">Color</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="reg_no" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->reg_number}} @endif">
                                                            <label class="form-label">Registration Number</label>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="make" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->make}} @endif">
                                                            <label class="form-label">Make/Model</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="type" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->type}} @endif">
                                                            <label class="form-label">Type</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="year" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->year}} @endif">
                                                            <label class="form-label">Year</label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="file"  class="form-control" name="picture"  id="quantity" >

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="chassis" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->chassis_number}} @endif">
                                                            <label class="form-label">Chassis Number</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <select class="form-control show-tick" name="status" >
                                                                <option value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->status}} @else {{$vehicles->status}}  @endif">@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->status}} @else {{$vehicles->status}} @endif </option>
                                                                <option>Available</option>
                                                                <option>Sold</option>
                                                                <option>Damaged</option>


                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group form-float">

                                                        <button class="btn btn-success btn-lg">Save </button>

                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="card">
                <div class="header">
                    
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">


                    @if($vehicles->vehicle_c)
                        <img src="{{url('').'/'.$vehicles->vehicle_c->image}}" style="width: 270px;height: 150px">
                    @else
                        <img src="{{url('').'/driver_image/example.jpg'}}" style="width: 270px;height: 150px">
                    @endif
                </div>

                    <div style="text-align: center">
                        <h3>
                    Toyota Corolla
                        </h3>
                        <h6>
                            @if($assign)
                                {{$assign->driver_name}}
                            @endif
                        </h6>

                        <b style="font-size: 11px; color: green">
                            Vehicle Status:
                            @if($vehicles->vehicle_c)
                            @if($vehicles->vehicle_c->status == 'Available')
                                <span class="label label-info">{{$vehicles->vehicle_c->status}}</span>
                            @else
                                <span class="label label-danger">{{$vehicles->vehicle_c->status}}</span>
                            @endif
                            @endif

                        </b>

                        <p><button class="btn btn-default btn-xs" data-toggle="modal" data-target="#status">  Change Status</button>
                        <p>Total Number of Trips: <span class="label label-primary">15</span>
                        <p>
                            <button class="btn btn-info btn-lg" data-toggle="modal" data-target="#details">
                                View Details
                            </button>
                        </p>




                        <br><br>
                </div>


            </div>
        </div>

</div>

    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Assignment</h4>
                </div>
                <form method="post" action="{{route('assign_vehicle')}}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <input type="hidden" name="vehicle_id" value="{{$vehicles->id}}">
                    <input type="hidden" name="vehicle_name" value="{{$vehicles->name}}   ">
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-5">
                           Vehicle Name: {{$vehicles->name}}
                        </div>
                        <div class="col-sm-1">

                        </div>

                        <div class="col-sm-6">
                            <select class="form-control show-tick" name="driver_id" data-live-search="true">
                                <option value="">-- Please select --</option>
                                @foreach($drivers as $driver)
                                    <option value="{{$driver->user_id}}">{{$driver->name}} @if($driver->status)  => <spam class="label label-info">{{$driver->status}}</spam>  @endif </option>
                                @endforeach

                            </select>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect">Assign</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="status" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Update Status <b class="label label-default"> {{$vehicles->status}} </b> </h4>
                    <hr>
                </div>
                <br>
                <form method="post" action="{{route('update_status_2')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <b>Vehicle Name: </b>  {{$vehicles->name}}
                                <input type="hidden" value="{{$vehicles->id}}" name="id">
                            </div>
                            <div class="col-sm-1">

                            </div>

                            <div class="col-sm-6">
                                <select class="form-control show-tick" name="status">
                                    <option value="">-- Select Status --</option>
                                    <option value="Available">Available</option>
                                    <option value="Sold">Sold</option>
                                    <option value="Damaged">Damaged</option>


                                </select>
                            </div>


                        </div>
                        <br><br>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Update</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="details" tabindex="-1" role="dialog">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Vehicle Details</h4>
                    <Hr>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row clearfix">

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="{{$vehicles->name}}" readonly>
                                    <label class="form-label">Vehicle Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->color}} @endif" readonly>
                                    <label class="form-label">Color</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->make}} @endif" readonly>
                                    <label class="form-label">Model</label>
                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="row clearfix">

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->type}} @endif" readonly>
                                    <label class="form-label">Type</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->year}} @endif" readonly>
                                    <label class="form-label">Year</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->chassis_number}} @endif" readonly>
                                    <label class="form-label">Chassis Number</label>
                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="row clearfix">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->reg_number}} @endif" readonly>
                                    <label class="form-label">Reg. Number</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="@if($vehicles->vehicle_c) {{$vehicles->vehicle_c->status}} @endif" readonly>
                                    <label class="form-label">Status</label>
                                </div>
                            </div>
                        </div>





                    </div>



                    <br>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection