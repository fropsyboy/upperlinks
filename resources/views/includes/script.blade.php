<!-- Jquery Core Js -->




<script src="{{url('assets/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{url('assets/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{url('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{url('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<script src="{{url('assets/plugins/dropzone/dropzone.js')}}"></script>
<script src="{{url('assets/plugins/nouislider/nouislider.js')}}"></script>
<!-- Waves Effect Plugin Js -->
<script src="{{url('assets/plugins/node-waves/waves.js')}}"></script>
<script src="{{url('assets/plugins/multi-select/js/jquery.multi-select.js')}}"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{url('assets/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

<!-- Custom Js -->
<script src="{{url('assets/js/admin.js')}}"></script>
<script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{url('assets/js/pages/forms/basic-form-elements.js')}}"></script>
<!-- Demo Js -->
<script src="{{url('assets/js/demo.js')}}"></script>