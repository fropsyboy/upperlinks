<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> Upper Links</title>
    <!-- Favicon-->
    <link rel="icon" href="{{url('assets/favicon.ico')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->


    <!-- Waves Effect Css -->
    <!-- Bootstrap Core Css -->
    <link href="{{url('assets/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{url('assets/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{url('assets/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <link href="{{url('assets/plugins/dropzone/dropzone.css')}}" rel="stylesheet">

    <link href="{{url('assets/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">

    <link href="{{url('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{url('assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <link href="{{url('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <link href="{{url('assets/plugins/nouislider/nouislider.min.css')}}" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{url('assets/css/themes/all-themes.css')}}" rel="stylesheet" />



</head>
