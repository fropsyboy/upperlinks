<aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{url('assets/images/user.png')}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi,@if(Auth::check())
                            {{ Auth::user()->name }}
                            @else
                        Applicant
                       @endif

                    </div>
                    <div class="email">
                        @if(Auth::check())
                        {{ Auth::user()->email }}
                    @else
                            Applicant
                        @endif
                    </div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>

                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="material-icons">input</i>Sign Out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>

                    <li @yield('userActive')>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Candidate Application</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="" >
                                    <span>Apply</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('login')}}" target="blank">
                                    <span>Admin</span>
                                </a>
                            </li>


                        </ul>
                    </li>
                    @if(Auth::check())

                    <li >
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Candidate Application</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="" >
                                    <span>View All</span>
                                </a>
                            </li>


                        </ul>
                    </li>
                        @endif

                    <li >
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Question 2</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{route('q2')}}" >
                                    <span>View</span>
                                </a>
                            </li>


                        </ul>
                    </li>



                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2019
                        Personal Projects
                </div>
            </div>
            <!-- #Footer -->
        </aside>
