@extends('layouts.master')
@section('userActive')
    class = 'active'
@endsection
@section('content')
    @if(Session::has('msg'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Alert...!</strong> {{ Session::get('msg', '') }}
        </div>
    @endif
    @if(Session::has('err'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Alert...!</strong> {{ Session::get('err', '') }}
        </div>
    @endif

    <div class="row clearfix">
        <form action="{{route('apply-form')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="card">
                <div class="header">
                    <h2>
                        User Management

                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">

                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="first" placeholder="First Name" value=""  required/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="surname" placeholder="Surname" value=""  required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone Number"value=""  required/>
                                </div>
                            </div>
                        </div>




                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Email" name="email" value="" required/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Cover Letter</label>
                                            <div class="form-line">
                                                <textarea class="form-control" name="cover" placeholder="Cover Letter">

                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Passport</label>
                                            <input type="file" class="form-control"  name="passport"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Resume</label>
                                            <input type="file" class="form-control"  name="resume"  required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button class="btn btn-info">Apply </button>
                            </div></div></div>
                </div>
            </div>
        </div>
        </form>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="card">
                <div class="header">

                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">

                        <img src="{{url('').'/driver_image/user.jpg'}}" style="width: 270px;height: 200px">

                </div>

                <div style="text-align: center">
                    <h3>

                    </h3>
                    <h6>

                    </h6>
                    {{--<h6>@if($driver->name) {{$driver->name}} @endif</h6>--}}
                    <b style="font-size: 11px; color: green">
                        Candidate's Status: Pending


                    </b>








                    <br><br>
                </div>


            </div>
        </div>
    </div>

@endsection
