@extends('layouts.master')
@section('userActive')
    class = 'active'
@endsection
@section('content')

    <div class="container-fluid" >

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                <button class="btn btn-info btn-xs waves-effect m-r-20" data-toggle="modal" data-target="#addUser" style="margin-left: 20px; margin-top: 20px;">Add User</button>

                <div class="body table-responsive">

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th style="width:30px;text-align: center;">
                                S/N
                            </th>
                            <th style="text-align: center;">
                                Image
                            </th>
                            <th style="text-align: center;">
                                Name
                            </th>
                            <th>
                                Surname
                            </th>
                            <th style="width:200px;text-align: center;">
                                Email
                            </th>
                            <th style="width:200px;text-align: center;">
                                Phone Number
                            </th>

                            <th>
                                Status
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach ($candidates as $user)


                            <tr>
                                <td style="width:30px;text-align: center;">
                                    {{$i}}
                                </td>
                                <td>

                                </td>
                                <td style="text-align: center;">
                                    {{$user->f_name}}
                                </td>
                                <td style="width:200px;text-align: center;">
                                    {{$user->s_name}}
                                </td>
                                <td style="width:200px;text-align: center;">

                                    {{$user->email}}
                                </td>
                                <td>
                                    {{$user->phone_number}}
                                </td>
                                <td>
                                    @if($user->status)
                                        <span class="label label-success"> {{$user->status}} </span>
                                    @else
                                        <span class="label label-default"> Pending </span>
                                    @endif
                                </td>
                                <td>
                                        <a href="" style="color:red; ">
                                            <button class="btn btn-info btn-xs">
                                                View Resume
                                            </button>
                                        </a>
                                    &nbsp;
                                    <a href="" style="color:red; ">
                                        <button class="btn btn-primary btn-xs">
                                            View Cover Letter
                                        </button>
                                    </a>

                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
